# Beat Tool

### :orange_book: Description

Beat Tool is an isolated module written on Python 3.6 that provides a simple
functionality to ingest app beats into an elasticsearch index.

Note that, you can use it to ingest only successful operations, or you can ingest
also those that failed. See usage section.

### :bookmark: Glossary

- Elasticsearch terms: those can be found and modified at `config/config.json`

| term | definition |
|---|---|
| beat | Elasticsearch index name |
| timestamp* | Date value to be used as timestamp for the index.|
| tool | The name of the tool which beat is being ingested.|
| status | An integer to be added in visualization calculation. |


### :heavy_plus_sign: Dependencies

Install requierements.txt file via
```bash
pip3 install -r requirements.txt -u
```

### :rocket: Usage

- Only success operations:

```python
from beattool.app import __run__

def run():
    # TODO
    # .
    # .
    # .
    __run__(tool="tool_name", status=1)
```

- Success and failed operations, use 1/0 as True False in boolean.

```python
from beattool.app import __run__

def run():
    try:
        # TODO
        # .
        # .
        # .
        __run__(tool="tool_name", status=1)
    except Exception: 
        __run__(tool="tool_name", status=0)
    
```

