# !/usr/bin/Python3.6

from pathlib import Path
import json

CONFIG_PATH = "/config/config.json"


def open_config_file():
    """
    Opens the configuration file stored at config folder.

    :return: a json with the configuration parameters
    """
    file_path = str(Path(__file__).parent.parent) + CONFIG_PATH
    try:
        with open(file_path) as f:
            return json.load(f)
    except OSError as exception:
        print(exception)
    except json.JSONDecodeError as jsonError:
        print(jsonError)
