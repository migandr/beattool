# !/usr/bin/Python3.6
from beattool.classes import ElasticSearch
import json


def __run__(tool, status):
    """
    Beat Tool main function. Makes a registry on the ES index with the given parameters

    :param tool: tool name for the index, must be added also the readme file
    :param status: 1/0 depending on the implementation you decided.
    """
    es = ElasticSearch.ElasticSearch()
    es.es.index(index=es.config_file['index'],
                doc_type="beat",
                body=json.dumps({"timestamp": es.ingested_date,
                                 "tool": tool,
                                 "status": status}))
