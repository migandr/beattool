# !/usr/bin/Python3.6

from elasticsearch import Elasticsearch
from beattool.utils.open_configuration_file import open_config_file
from datetime import datetime
import pytz


class ElasticSearch(object):

    def __init__(self):
        """
        ES Builder to perform all operations.

        Configuration must be provided at config/config.json. This file is self describable.
        """
        self.config_file = open_config_file()
        self.es = Elasticsearch([
            {"host": self.config_file["host"],
             "port": self.config_file["port"]
             }
        ])
        self.__set_index__()
        self.ingested_date = self.__set_ingested_date__()

    def __set_index__(self):
        """
        Setter for the first time an index is provided from config file.
        """
        if self.config_file["index"] not in self.__get_indices__():
            self.es.indices.create(index=self.config_file["index"],
                                   body=self.config_file["_mapping"],
                                   request_timeout=30)

    def __get_indices__(self):
        """
        Get all the indices from the elasticsearch server.
        :return: all the indices.
        """
        return self.es.indices.get('*')

    def __set_ingested_date__(self):
        """
        Using the Python Timezone library, gets the timezone configured in the timezone parameter
        in the config file.

        :return: a string representation adapted to ES format yyyy-MM-dd HH:mm:ss
        """
        timestamp = datetime.now(tz=pytz.timezone(self.config_file["timezone"])).timestamp()
        timestamp = str(datetime.fromtimestamp(timestamp)).split(".")
        return timestamp[0]

    def es_put_mapping(self, body, doc_type):
        """
        Put a new field in the mapping of the index defined in the configuration file using the following configuration
        for the body:

        {
            "properties": {
                "tcOutputDialog": {
                    "type": "text"
                }
            }
        }

        :param body: a json with the previous information
        :param doc_type: the name of the doc type, in this example should be "item"

        :return: print the result
        """
        print(self.es.indices.put_mapping(doc_type=doc_type,
                                           index=self.config_file["index"],
                                           body=body))

